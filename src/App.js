import image from "./assets/images/48.jpeg"
const diveStyle = {
  backgroundColor: "bisque",margin: "50px  30px" ,textAlign: "center",border: "solid"
}
const avatar = {
  borderRadius: "50%",marginTop: "-50px"
}
const quote = {fontStyle: "italic"}
const brown = { color: "brown"}
const blue = { color: "blue"}

function App() {
  return (
    <div style={diveStyle}>
      <img src={image} style={avatar} alt="avatar"></img>
      <p style={quote}>This is the one of the best developer blogs on the planet! I read it daily to improve my skills</p>
      <p style={brown}><b style={blue}>Tammy Stevens </b>Front End Developer</p>
    </div>
  );
}

export default App;
